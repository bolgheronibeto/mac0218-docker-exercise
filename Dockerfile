FROM caddy

RUN mkdir /usr/src
RUN mkdir /usr/src/pages

COPY index.html /usr/src/pages

COPY Caddyfile /etc/caddy/Caddyfile